import { useEffect, useState } from "react";

function shuffle(array) {
    let currentIndex = array.length,  randomIndex;

    // While there remain elements to shuffle.
    while (currentIndex !== 0) {

        // Pick a remaining element.
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }

    return array;
}

function createArray(elements) {
    return [...Array(elements).keys()];
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function swap(array, a, b) {
    let stored = array[a]
    array[a] = array[b]
    array[b] = stored
    return array
}



export default function useRandomArray(elements) {
    const [numbers, setnumbers] = useState([]);
    const [n, setn] = useState(0);


    async function bubbleSort (array, idx) {
        if (idx === array.length) return;
        let sub = 0;
        while (sub < array.length - idx - 1) {
            if(array[sub] > array[ sub + 1]){
                swap(array, sub, sub + 1);
            }
            setn(sub);
            sub ++;
        }

        setnumbers(array)

        await sleep(200)

        return bubbleSort(array, idx + 1);
    }

    async function insertionSort(array, idx) {
        if (idx >= array.length - 1) return;
        let next_idx = idx + 1;
        if (array[next_idx] < array[idx]) {
            while (next_idx > 0) {
                if (array[next_idx] < array[next_idx - 1]) {
                    swap(array, next_idx, next_idx - 1);
                    next_idx -= 1;
                } else {
                    next_idx= 0;
                    break
                }
                setn(next_idx)
            }
        }
        setnumbers(array)
        await sleep(200)

        return insertionSort(array, idx + 1)

    }

    async function quickSort(array, start, end) {
        if ( start >= end ) return;
        let index =  await partition(array, start, end);
        setn(index);
        Promise.all([quickSort(array, start, index - 1), quickSort(array, index + 1, end)]);
    }

    async function partition(array, start, end) {
        let pivotIndex = start;
        let pivotValue = array[end];

        for (let i = start; i < end; i ++) {
            if (array[i] < pivotValue) {
                swap (array, i, pivotIndex);
                pivotIndex ++;
            }
        }
        swap(array, pivotIndex, end);
        await sleep(200);
        return pivotIndex
    }

    async function siftDown (array, currentIndex, endIndex) {
        let childOneIndex = currentIndex * 2 + 1;
        while (childOneIndex <= endIndex) {
            let childTwoIndex = currentIndex * 2 + 2 < endIndex ? currentIndex * 2 + 2 : -1;
            let indexToSwap;
            if (childTwoIndex !== -1 && array[childOneIndex] > array[childTwoIndex]) indexToSwap = childOneIndex;
            else indexToSwap = childTwoIndex;

            if (array[indexToSwap] > array[currentIndex]) {
                swap(array, currentIndex, indexToSwap);
                currentIndex = indexToSwap;
                childOneIndex = currentIndex * 2 + 1;

            } else return;


        }


    }

    async function buildMaxHeap(array) {
        const lastNoneLeftNodeIndex = Math.floor((array.length - 2) / 2);
        for (let currentIndex = lastNoneLeftNodeIndex; currentIndex >= 0; currentIndex --){
            siftDown(array, currentIndex, array.length - 1);
        }
    }

    async function heapSort(array) {
        buildMaxHeap(array);
        for (let endIndex = array.length - 1; endIndex > 0; endIndex --) {
            swap(array, 0, endIndex);
            siftDown(array, 0, endIndex);

            await sleep(200);
            setn(endIndex);

        }

    }

    useEffect(() => {
        setnumbers(shuffle(createArray(elements)));
    }, [])

    return [numbers, bubbleSort, insertionSort, quickSort, heapSort]
}
