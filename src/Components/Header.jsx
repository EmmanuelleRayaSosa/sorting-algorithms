import React from 'react'

export default function Header() {
    return (
        <header>
            <div className="container">
                <div className="row p-1">
                    <div className="col-6"><strong>Emmanuelle Code</strong></div>
                    <div className="col-6"></div>
                </div>
            </div>
        </header>
    )
}
