import React from 'react'

export default function Bar({number}) {
    return (
        <div style={{width: '15px', height: number + '%'}}></div>
    )
}
