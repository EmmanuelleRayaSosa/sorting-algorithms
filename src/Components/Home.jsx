import React from 'react'
import Bar from './Bar';
import useRandomArray from '../Hooks/useRandomArray';

export default function Home() {
    let [numbers, bubbleSort, insertionSort, quickSort, heapSort] = useRandomArray(30);
    return (
        <>
        <div className='container'>
            <div className='row'>
                <div className='col-6'></div>
                <div className='col-6'>
                <div className='buttons'>
                    <button className='btn btn-primary btn-sm' onClick={() => bubbleSort(numbers, 0)}>bubblesort</button>
                    <button className='btn btn-primary btn-sm' onClick={() => insertionSort(numbers, 0)}>insertionsort</button>
                    <button className='btn btn-primary btn-sm' onClick={() => quickSort(numbers, 0, numbers.length)}>quickSort</button>
                    <button className='btn btn-primary btn-sm' onClick={() => heapSort(numbers)}>heapSort</button>
                </div>
                </div>
            </div>
        </div>
        
        <div className='screen'>
            {numbers.map(number => <Bar key={number} number={number}/>)}
        </div>
        </>
        
    )
}
