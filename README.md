# Sorting Algorithms

- Node > 16
- npm 8.13.0

This project was created using `npm create-react-app sorting-algorithms` 

For development mode run `npm i` for the first time. If you want yo run the node server, use

```bash
npm start
```

For creating a productive version of the code run:

```bash
npm build
```